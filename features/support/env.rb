require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'pry'
require 'minitest/spec'

#require 'sauce'
#require 'sauce/cucumber'
HOST='https://us2.stg.usw2.spny.us'
Capybara.configure do |config|
  config.run_server = false
  config.default_driver = :selenium
  config.app_host = HOST
end
World(MiniTest::Assertions)
MiniTest::Spec.new(nil)

#After { Capybara.quit }
#STAGING= HOST.include? 'us2.stg.spny' ? true :false
