Feature: As a diner with a gnawing need to express opinion
  on a restaurant I'd like to sign up and write a review

#Fails because not yet deployed
@workflow @failing
Scenario: Search for a restaurant on google and land on urbanspoon page
  Given I am on google search page
    And I search for Starbucks SODO urbanspoon
  When I select urbanspoon url
  Then I should be on Starbucks, SODO page

@workflow @passing
Scenario: Vote on a restaurant, assuming user is already signed in
  Given I am signed in as an existing user
    And Diner on Starbucks, SODO page
  When I click on upvote on restaurant page
  Then My vote should be counted
  When I click on upvote on restaurant page
  Then My vote should be cleared

@workflow @failing
Scenario: Write review after signup
  Given Diner on Starbucks, SODO page
    And I click on Add a review on restaurant page
  Then I should be taken to signup page
    And I click on create your account on signup page
  Then I should be taken to account create page
  When I enter my email, password, confirm password
    And I click Create my account on create page
  When I should be taken to review page
    And I enter my review and click add review
  Then I should see my review added

@workflow @failing @cleanup
Scenario: Add a restaurant to favorite
  Given Diner on Starbucks, SODO page
    And I click on Save to Wishlist on restaurant page
  Then I should be taken to signup page
    And I enter email, password of an existing user
    And I click on Sign In on signin page
  Then I should be on Starbucks, SODO page
    And Starbucks, SODO should be added to my favorites
