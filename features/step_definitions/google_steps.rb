Given(/^I am on google search page$/) do
  visit 'http://www.google.com'
end

When(/^I search for (.*)/) do |search_terms|
  fill_in 'q', :with => search_terms
  find('[name~=btnG]').click
end
When(/^I select urbanspoon url$/) do
  #NOTE - this could be fragile
  has_selector? 'h3'
  first('h3>a').click
end
