Then(/^I should be taken to signup page$/) do
  page.find('#username')
  page.current_url.must_include '/u/signin'
end
When(/^I enter email, password of an existing user$/) do
  fill_in 'username', :with => 'satya+json@urbanspoon.com'
  fill_in 'password', :with => 'password'
end
When(/^I click on Sign In on signin page$/) do
  click_button 'Sign In'
end
When(/^I am signed in as an existing user$/) do
  visit '/'
  within('#user') { click_link 'Sign In' }
  fill_in 'username', :with => 'satya+json@urbanspoon.com'
  fill_in 'password', :with => 'password'
  click_button 'Sign In'
end