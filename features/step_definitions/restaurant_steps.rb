When(/^I click on Add a review on restaurant page$/) do
  click_link 'Add a review'
end
When(/^Diner on Starbucks, SODO page$/)do
  #TODO Pick from a random restaurant
  visit '/r/1/1349486/restaurant/Sodo/Starbucks-Seattle'
end
When(/^I click on Save to Wishlist on restaurant page$/) do
  click_link 'Save to Wishlist'
end
Then(/^I should be on Starbucks, SODO page$/) do
  #TODO better verification
  #Make sure this is the new design, old design has this class
  has_no_selector? '.you_voted'
  page.find('h1').must_include 'Starbucks'
  page.find('span').must_include 'Sodo'
end
#NOTE: this @initial_text is being called twice
When(/^I click on upvote on restaurant page$/) do
  @initial_text = page.find( 'div.total').text
  page.find( 'i.like').click
end
Then(/^My vote should be counted$/) do
  #Wait till the vote shows up as selected
  has_selector?('.like.selected')
  @after_text = page.find( 'div.total').text
  @after_text.wont_match @initial_text
end
Then(/^My vote should be cleared$/) do
  has_no_selector?('.like.selected')
  @final_text = page.find( 'div.total').text
  @final_text.wont_match @after_text
end
